name := "ws-server"

version := "0.1"

scalaVersion := "2.12.6"

val akkaVersion = "2.5.12"
val akkaHttpVersion = "10.1.5"
val circeV = "0.9.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka"  %% "akka-cluster-tools" % akkaVersion,
  "io.circe" %% "circe-core" % circeV,
  "io.circe" %% "circe-generic" % circeV,
  "io.circe" %% "circe-parser" % circeV
)